// Flight logging code
// For AU USLI Fall 2018 subscale launch 1
// Embedded team
// Written for the STM32F103C8 using STM32DUINO libraries

// Modules used:
// 'ADXL345 3-axis accelerometer
// 'MPL3115A2 altimeter
// 'Adafruit MicroSD breakout board

// Power connections
// Supply the 5V pin (next to PC13 LED) with clean 5v
// The 3.3v pin is an output
// VB (Vbatt) has a maximum of 5.5v input, and has not been tested

// SPI connections
// SPI1 (SD card, 5v)
// SS    <-->  PA4
// SCK   <-->  PA5
// MISO  <-->  PA6
// MOSI  <-->  PA7

// SPI2 (Accelerometer, 5v to Vin)
// SS    <-->  PB12
// SCK   <-->  PB13
// MISO  <-->  PB14
// MOSI  <-->  PB15

// I2C connections
// I2C1 (Altimeter, 5v to Vin)
// SCL  <--> PB6 (10k to 5v pullup)
// SDA  <--> PB7 (10k to 5v pullup)

#include <SD.h>
#include <SPI.h>
// #include <Wire.h>
#include <stdarg.h>
#include <stdio.h>

/* ALL THE CONSTANTS THE CONTROLLER NEEDS TO FUNCTION */
/* KP, KI and KD gathered from Simulink simulations */
#define GAINFACTOR 0.01
#define KP 1 * GAINFACTOR    // Proportional gain.
#define KI 0.5 * GAINFACTOR  // Integral gain.
#define KD 5 * GAINFACTOR    // Derivative gain.
#define I_MAX 100            // Prevents integral windup.
#define SETPOINT 4700        // Target altitude in feet.
#define MAX 1                // Maximum input to motor.
#define MIN 0                // Minimum input to motor.
#define METERTOFEET 3.280839895
#define GRAV 32.17405  // gravity in english units
#define TIMESTEP 0.1   // TODO: <- THIS NEEDS TO BE CHANGED

typedef struct {
    size_t nrows;
    size_t ncols;
    double **elems;
} Matrix;

typedef enum { ROW, COL } VectorType;

enum Runmode {
    idle,    // Rocket is sitting on launchpad
    launch,  // Motor is ignited and burning (have to wait for burnout)
    coast,   // Motor is burnt out, this is where the plates deploy
    descent  // Rocket is now descending - retract fairings and power down
};

/**
 * Basic matrix constructor, which takes in the number of
 * rows and columns, and a variadic list of elements. The
 * elements are added by row, so it will fill the first
 * row element by element and move onto the second row, etc
 */
Matrix *matrix(size_t nrows, size_t ncols, ...) {
    Matrix *mat = (Matrix *)malloc(sizeof(Matrix));
    mat->nrows = nrows;
    mat->ncols = ncols;
    // Allocating rows
    mat->elems = (double **)malloc(nrows * sizeof(double *));
    // Allocating columns
    for (size_t i = 0; i < nrows; ++i) {
        mat->elems[i] = (double *)malloc(ncols * sizeof(double));
    }
    va_list elements;
    va_start(elements, ncols);
    for (size_t i = 0; i < nrows; ++i) {
        for (size_t j = 0; j < ncols; ++j) {
            mat->elems[i][j] = va_arg(elements, double);
        }
    }
    va_end(elements);
    return mat;
}

Matrix *matrix_with_fill(size_t nrows, size_t ncols, double fill) {
    Matrix *mat = (Matrix *)malloc(sizeof(Matrix));
    mat->nrows = nrows;
    mat->ncols = ncols;
    // Allocating rows
    mat->elems = (double **)malloc(nrows * sizeof(double *));
    // Allocating columns
    for (size_t i = 0; i < nrows; ++i) {
        mat->elems[i] = (double *)malloc(ncols * sizeof(double));
    }
    for (size_t i = 0; i < nrows; ++i) {
        for (size_t j = 0; j < ncols; ++j) {
            mat->elems[i][j] = fill;
        }
    }
    return mat;
}

/**
 * Multiply two matrices and return a new one
 */
Matrix *matrix_mult(Matrix *mat1, Matrix *mat2) {
    if (mat1 == NULL) {
        printf("First Matrix in matrix_mult is NULL\n");
        return NULL;
    }
    if (mat2 == NULL) {
        printf("Second Matrix in matrix_mult is NULL\n");
        return NULL;
    }
    if (!can_multiply_matrices(mat1, mat2)) {
        printf("Cannot multiply matrices\n");
        printf("Matrix one has: %zd columns\n", mat1->ncols);
        printf("Matrix two has: %zd rows\n", mat2->nrows);
        return NULL;
    }
    Matrix *new_mat = matrix_with_fill(mat1->nrows, mat2->ncols, 0.0);
    for (size_t i = 0; i < mat1->nrows; ++i) {
        for (size_t j = 0; j < mat2->ncols; ++j) {
            double value = 0;
            for (size_t k = 0; k < mat1->ncols; ++k) {
                value += mat1->elems[i][k] * mat2->elems[k][j];
            }
            new_mat->elems[i][j] = value;
        }
    }
    return new_mat;
}

bool can_multiply_matrices(Matrix *mat1, Matrix *mat2) {
    return mat1->ncols == mat2->nrows;
}
/**
 * Static scopes the values to this file only, so it keeps them contained
 * within and is not accesible outside of vehicle.cpp
 */
static double P;
static double I;
static double D;
static double P_0;

double get_accel() { return -32.174; }

void HELLO() { Serial.println("HELLO, WORLD!"); }

bool verify_launch() { return true; }

double projected_altitude(double veloc, double accel, double currentAlt) {
    double veloc_sqr = veloc * veloc;
    return (-veloc_sqr / (2 * (accel + GRAV))) * log(-accel / GRAV) +
           currentAlt;
}

double pid(double position, double proj_alt) {
    P = proj_alt - SETPOINT;  // Proportional term.

    I += P * TIMESTEP;

    D = (P - P_0) / 2;  // Derivative term.

    // diffOutput is the change in plate position, so we add it to the old
    // position.
    double diffOutput = (P * KP) + (I * KI) + (D * KD);

    // This block saves the variables in this loop so it can be used in the
    // next.
    P_0 = P;

    // updating the position of the plates.
    position += diffOutput;

    // These prevent the new position of the plates from going over their max
    // and min values.
    if (position > MAX)
        position = MAX;
    else if (position < MIN)
        position = MIN;

    return position;
}
// File object
Runmode MODE = idle;

void setup() { Serial.begin(9600); }

void loop() { HELLO(); }
