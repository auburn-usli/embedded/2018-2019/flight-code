
#include "flightcode.h"

/**
 * This class does a good job of filtering velocity and alitude but
 * it's struggling a bit to filter the acceleration. It does a good
 * job of smoothing the signal but it returns values that are a bit
 * higher than the true value so the projected altitude equation is
 * a bit off.
 */

// Private method that saves the current variables to be used in the next
// iteration.
void Kalman::update(double z, double u, double a) {
    x_k += (v_k * dt) + (0.5 * a * (dt * dt));  // Calculate the new altitude
    v_k += (a * dt);                            // Calculate the new velocity
    P_k += Q;                                   // Compute the new covariance
    K_k = P_k / (P_k + R);  // Compute the Kalman gain for this iteration
    x_t =
        x_k + (K_k * (z - x_k));  // Calculate the new filtered altitude output
    v_t =
        v_k + (K_k * (u - v_k));  // Calculate the new filtered velocity output
    P_t = (1 - K_k) * P_k;        // Calculate new error covariance
    P_k = P_t;                    // Set error covariance for next iteration
    x_k = x_t;                    // Set the old output for the next iteration
    k += 1;
}

// Wip
void Kalman::update_accel(double a_k) {
    K_k = P_k / (P_k + R);  // Compute the Kalman gain for this iteration.
    a_t = a_k + (K_k * (a_k - a_k));  // Calculate the new filtered output
    P_t = (1 - K_k) * P_k;            // Calculate new error covariance
    P_k = P_t;                        // Set error covariance for next iteration
    a_k = a_t;
}

// Constructor
Kalman::Kalman(double sensor_noise, double covariance, double environ_uncert,
               double time_step, double init_alt, double init_veloc,
               double init_accel) {
    R = sensor_noise;
    Q = environ_uncert;
    P_k = covariance;
    x_k = init_alt;
    v_k = init_veloc;
    a_k = init_accel;
    dt = time_step;
}

Kalman::Kalman(double sensor_noise, double covariance) {
    this->R_a = sensor_noise;
    this->a_k = -32.174;
    this->P_k_1_a = covariance;
}

// Use this method when we want to get the filtered output
State Kalman::output(double z, double u, double a) {
    update(z, u, a);
    update_accel(a);
    State state;
    state.accel = a_t;
    state.veloc = v_t;
    state.alt = x_t;
    return state;
}