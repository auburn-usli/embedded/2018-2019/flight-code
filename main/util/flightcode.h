
/**
 * MASTER HEADER FILE
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef VEHICLE_H
#define VEHICLE_H

/**
 * Runmode controls the flow of the main loop; -> allows switching between
 * states
 */
// This is our max altitude, which is what the pid controller uses to calculate
// the position of the plates. You can find the equations used here:
// https://www.grc.nasa.gov/www/k-12/airplane/flteqs.html.
double projected_altitude(double veloc, double accel, double currentAlt);

void HELLO();

// The actual controller and functions
double pid(double position, double proj_alt);

/**
 * Return values from sensors, calculate it in the case of get_veloc()
 */
double get_accel();
double get_veloc(double accel, double veloc);
double get_alt(double veloc, double alt);

/**
 * The reason this function is here is to check if the rocket is actually
 * launching, instead of it falling off the launchpad somehow, etc
 */
bool verify_launch();

/* Double check rocket has burned out */
bool verify_burnout();

#endif  // VEHICLE_H

#ifndef FILTER_H
#define FILTER_H

/**
 * The Kalman class uses the State struct to return one object
 * that has the filtered values.
 */
typedef struct {
    double alt;
    double veloc;
    double accel;
} State;

/**
 * This class does a good job of filtering velocity and alitude but
 * it's struggling a bit to filter the acceleration. It does a good
 * job of smoothing the signal but it returns values that are a bit
 * higher than the true value so the projected altitude equation is
 * a bit off.
 */
class Kalman {
   private:
    double K_k;      // Kalman gain
    double P_k;      // Error covariance
    double P_t;      // Combined covariance
    double R;        // Covariance, or noise
    double Q;        // Environmental uncertainty
    double x_k;      // Altitude calculation
    double x_t;      // Combined altitude
    double v_k;      // Velocity calculation
    double v_t;      // Combined velocity
    unsigned int k;  // Number of iterations
    double dt;       // Change in time

    double a_t;      // Combined acceleration
    double R_a;      // Covariance, or noise
    double P_k_1_a;  // Error covariance from the previous iteration
    double a_k;

    // Private method that saves the current variables to be used in the next
    // iteration.
    void update(double z, double u, double a);

    // Wip
    void update_accel(double a_k);

   public:
    // Constructor
    Kalman(double sensor_noise, double covariance, double environ_uncert,
           double time_step, double init_alt, double init_veloc,
           double init_accel);

    // Misc constructor
    Kalman(double sensor_noise, double covariance);

    /**
     * Inputs are the unfiltered acceleration, velocity, and alitude,
     * outputs a State struct that has the filtered values
     */
    State output(double z, double u, double a);
};

#endif  // FILTER_H

/**
 * DATA LOGGING
 */

#ifndef _DATALOG_h
#define _DATALOG_h

#define NUMDATA 7
#define CHIPSELECT 10  // or whatever it's supposed to be

// Thinking we can add the data to this array while the loop is running
// and just clear it out after all the data is written
double data_array[NUMDATA];

// name of file
const char *fileName = "data.txt";

// All the names of the data points, which are the column names in the text file
const char *headers[] = {"Time (sec)",          "Acceleration (ft/s^2)",
                         "Velocity (ft/s)",     "Altitude (ft)",
                         "Proj. Altitude (ft)", "Raw Acceleration (m/s^2)",
                         "Raw Altitude (ft)"};

// Set up text file with headers and what not.
void initialize_file();

// Number of whole digits for each data point
int data_len(double data);

// Clear the current iteration's data so we can load the next iteration
void pop_data();

// pretty print text to file
void print_data();

#endif  // DATALOG_H

#ifndef LINALG_H
#define LINALG_H
/**
 * Main struct matrix functions depend on.
 *
 * It has a double pointer which points to two arrays. A Matrix's
 * elements can be accessed by doing: mat->elems[i][j]. If you use
 * the constructors, be aware that they manually allocate the memory
 * using malloc. If you use temporary matrices in a function, make
 * sure to free the memory before the function ends. Otherwise not
 * even God can help you.
 */

/**
 * Used internally for checking whether a matrix is a vector,
 * and what type of vector since some operations depend
 * on the vector being a row or column vector.
 */

Matrix *matrix(size_t nrows, size_t ncols, ...);

/**
 * Like matrix() but fills all elements with one value
 */
Matrix *matrix_with_fill(size_t nrows, size_t ncols, double fill);

/**
 * Construct 1 dimensional matrices, which can be used like vectors.
 * Functions will check to see if a matrix is actually a vector before
 * doing any vector operations.
 */
Matrix *row_vector(size_t ncols, ...);
Matrix *col_vector(size_t nrows, ...);

/**
 * Multiply two matrices together.
 */
Matrix *matrix_mult(Matrix *mat1, Matrix *mat2);

bool can_multiply_matrices(Matrix *mat1, Matrix *mat2);
#endif  // LINALG_H
